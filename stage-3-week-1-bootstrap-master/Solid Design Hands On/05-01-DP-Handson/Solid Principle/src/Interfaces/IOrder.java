package Interfaces;

public interface IOrder {

	void processOrder(String modelName);
}
