package Main;

import java.util.Scanner;

import Classses.AccessoryRepair;
import Classses.PhoneOrder;
import Classses.PhoneRepair;

public class Main {

	public static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Welcome to our site. Would you like to order or repair?");
		String inputType = sc.next();

		switch (inputType) {
		case "order":
			System.out.println("Please provide the phone model name");
			String modelName = sc.nextLine();
			PhoneOrder phoneOrder = new PhoneOrder();
			phoneOrder.processOrder(modelName);
			break;
		case "repair":
			System.out.println("Is it the phone or the accessory that you want to be repaired?");
			String productType = sc.next();
			if (productType.equalsIgnoreCase("phone")) {
				System.out.println("Please provide the phone model name");
				String phoneModelName = sc.nextLine();
				PhoneRepair phoneRepair = new PhoneRepair();
				phoneRepair.processPhoneRepair(phoneModelName);
			} else {
				System.out.println("Please provide the accessory detail, like headphone, tempered glass");
				String accessoryType = sc.nextLine();
				AccessoryRepair accessoryRepair = new AccessoryRepair();
				accessoryRepair.processAccessoryRepair(accessoryType);
			}
			break;
		default:
			break;
		}
	}

}
