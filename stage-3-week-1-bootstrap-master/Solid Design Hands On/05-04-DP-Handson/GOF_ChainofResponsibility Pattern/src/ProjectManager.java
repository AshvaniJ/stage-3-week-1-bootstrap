
public class ProjectManager implements ILeaveRequestHandler {

	public ILeaveRequestHandler nextHandler = new HR();
	
	@Override
	public void handleRequest(LeaveRequest leaveRequest) {
		// TODO Auto-generated method stub
		if(leaveRequest.leaveDays > 2 && leaveRequest.leaveDays < 6) {
			System.out.println(leaveRequest.employee + " your leave request for " + leaveRequest.leaveDays + " is process with Project Manager");
		}
		else {
			nextHandler.handleRequest(leaveRequest);
		}
	}

}
