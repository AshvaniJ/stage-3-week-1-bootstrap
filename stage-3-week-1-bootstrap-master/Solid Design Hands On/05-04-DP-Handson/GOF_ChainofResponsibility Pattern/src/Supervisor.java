
public class Supervisor implements ILeaveRequestHandler {

	public ILeaveRequestHandler nextHandler = new ProjectManager();
	
	@Override
	public void handleRequest(LeaveRequest leaveRequest) {
		// TODO Auto-generated method stub
		if(leaveRequest.leaveDays > 0 && leaveRequest.leaveDays < 3) {
			System.out.println(leaveRequest.employee + " your leave request for " + leaveRequest.leaveDays + " is in process with Supervisor");
		}
		else {
			nextHandler.handleRequest(leaveRequest);
		}
	}

}
