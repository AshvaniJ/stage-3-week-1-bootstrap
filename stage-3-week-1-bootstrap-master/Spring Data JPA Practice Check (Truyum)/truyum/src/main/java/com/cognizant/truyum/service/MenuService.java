package com.cognizant.truyum.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.truyum.model.Menu;
import com.cognizant.truyum.repository.MenuRepository;

@Service
public class MenuService {
	
	@Autowired
	MenuRepository menuRepository;

	@Transactional
	public void addMenuItem(Menu menu) {
		menuRepository.save(menu);
	}
	
	@Transactional
	public List<Menu> listOfMenuForCustomer() {
		return menuRepository.findByActiveTrueAndLaunchDateLessThanEqual(new Date());
	}
	
	@Transactional
	public List<Menu> listOfMenuForAdmin() {
		return menuRepository.findAll();
	}
}
