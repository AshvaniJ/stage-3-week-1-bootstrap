<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Menu List Admin</title>
<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="/css/style.css" rel="stylesheet">
</head>
<body>
	<!-- Nav Bar -->
	<nav class="navbar navbar-expand-sm navbar-dark"
		style="background-color: coral">
		<!-- Nav Bar Branding -->
		<a class="navbar-brand" href="menu-item-list.html"><span
			class="material-icons material-logo mr-2"> restaurant_menu </span>truYum</a>
		<!-- Nav Bar Toggle Buttom -->
		<button class="navbar-toggler d-lg-none" type="button"
			data-toggle="collapse" data-target="#collapsibleNavId"
			aria-controls="collapsibleNavId" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<!-- Nav Bar Options -->
		<div class="collapse navbar-collapse" id="collapsibleNavId">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active"><a class="nav-link"
					href="menu-item-list.html">Menu Items</a></li>
			</ul>
		</div>
	</nav>
	<!-- End Nav Bar -->

	<!-- Search Banner -->
	<div class="banner">
		<div class="pl-5 pt-5">
			<h1 class="text-white mt-4">Find Food</h1>
		</div>
		<div class="input-group md-form form-sm form-1 pl-5 pr-5">
			<div class="input-group-prepend">
				<span class="input-group-text purple lighten-3" id="basic-text1"><span
					class="material-icons"> local_cafe </span></span>
			</div>
			<input class="form-control my-0 py-1" type="text"
				placeholder="Search" aria-label="Search">
		</div>
	</div>

	<!-- Main Body -->
	<div class="container-fluid">
		<!-- First Row -->
		<div class="row mt-5 mb-5">

			<c:forEach var="menu" items="${menuList }">
				<div class="col-xs-12 col-md-3 col-sm-6">
					<div class="card">
						<img class="card-img-top" src="${menu.imageUrl }"
							alt="${menu.name }">
						<div class="card-body">
							<div class="row">
								<div class="col-8 text-left">
									<h5 class="card-title text-muted">${menu.name }</h5>
								</div>
								<div class="col-4 text-right">
									<p class="card-text text-danger">
										<i class="fa fa-inr"></i>${menu.price }
									</p>
								</div>
							</div>
							<div class="row">
								<div class="col-7 text-left">
									<p class="card-subtitle text-secondary">
										<small> 
										<c:if test="${menu.active }">
										<span class="badge badge-success">Active</span>
										</c:if>
										<c:if test="${!menu.active }">
										<span class="badge badge-danger ">InActive</span>
										</c:if>
											${menu.category }
										</small>
									</p>
								</div>
								<div class="col-5 text-right">
									<c:if test="${menu.freeDelivery }">
										<p class="card-text">
											<small> <span class="material-icons material-truck">
													local_shipping </span> <span
												class="badge badge-primary badge-free">FREE</span>
											</small>
										</p>
									</c:if>
								</div>
							</div>
							<div class="row">
								<div class="col-5 text-left">
									<small class="text-secondary"><span
										class="material-icons material-launch"> schedule </span>
										Launch <fmt:formatDate value="${menu.launchDate }" type="date"
											pattern="dd/MM/yyyy" /></small>
								</div>
								<div class="col-7 text-right mt-2">
									<button type="button" class="btn btn-sm text-white btn-edit"
										style="background-color: coral;">
										<span class="material-icons material-edit"> edit </span> Edit
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</c:forEach>

		</div>
		<!-- Main Container End -->
		<footer class="page-footer font-small position-bottom "
			style="background-color: coral">
			<div class="footer-copyright text-left py-1 ml-3 text-white">Copyright
				2019</div>
		</footer>
		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
			integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
			crossorigin="anonymous"></script>
		<script
			src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
			integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
			crossorigin="anonymous"></script>
		<script
			src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
			integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
			crossorigin="anonymous"></script>
</body>
</html>